# Lamotivo uploads

## Introduction

A Laravel package to manage uploads for your application.

Works with flowjs/flow-php-server.

## Installation

```
composer require lamotivo/uploads
```

Then, publish the config

```
php artisan vendor:publish --provider="Lamotivo\Uploads\UploadServiceProvider" --config
```

and the migration

```
php artisan vendor:publish --provider="Lamotivo\Uploads\UploadServiceProvider" --migrations
```

