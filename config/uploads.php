<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Attachment Eloquent model
    |--------------------------------------------------------------------------
    |
    | This model will be used to store uploaded files data.
    | You can also inherit your own model from our default model.
    |
    */

    'attachment_model' => Lamotivo\Uploads\Attachment::class,

    /*
    |--------------------------------------------------------------------------
    | User Eloquent model
    |--------------------------------------------------------------------------
    |
    | This model will be used to bind uploaded files to a user.
    |
    */

    'user_model' => App\Models\User::class,

    /*
    |--------------------------------------------------------------------------
    | User Morhping
    |--------------------------------------------------------------------------
    |
    | If an attachment can belong to only one model (e.g. App\User), you should
    | set this option to false.
    |
    | If an attachment can belong to different models, you should set this
    | option to true.
    |
    */

    'user_morphing' => false,

    /*
    |--------------------------------------------------------------------------
    | Default disk
    |--------------------------------------------------------------------------
    |
    | Specify a disk from your filesystems.
    | This disk must be present in disks option (see below).
    |
    */

    'disk' => 'uploads',


    /*
    |--------------------------------------------------------------------------
    | Allowed disks to upload to
    |--------------------------------------------------------------------------
    |
    | List all allowed disks. See `config/filesystems.php`.
    | 
    | Keys are disks names, values are optional properties might be
    | required by your application.
    |
    */

    'disks' => [
        'uploads' => [
            'name' => 'Uploads',
        ],
        // 'custom' => [
        //     'name' => 'Custom',
        //     'handler' => App\Uploader\CustomUploadHandler::class,
        //     'presize_disabled' => false,
        //     'presizes' => [
        //         '560x330' => [
        //             'width' => 560,
        //             'height' => 330,
        //             'crop' => true,
        //         ],
        //     ],
        // ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Max size for files
    |--------------------------------------------------------------------------
    |
    | This option might be required by your application.
    | The value is in kilobytes.
    |
    */

    'max_size' => 2048,


    /*
    |--------------------------------------------------------------------------
    | Resizing values
    |--------------------------------------------------------------------------
    |
    | Max sizes for images to automatically reduce image sizes to given values.
    | No upsize.
    |
    */

    'resize' => [
        'max_width'  => 1920,
        'max_height' => 1920,
    ],


    /*
    |--------------------------------------------------------------------------
    | Preview sizes
    |--------------------------------------------------------------------------
    |
    | Sizes for images to automatically create previews for image.
    | Image will be resized to given values, with no upsize, keeping aspect 
    | ratio.
    |
    */

    'preview' => [
        'width'  => 480,
        'height' => 480,
    ],


    /*
    |--------------------------------------------------------------------------
    | Thumbnail sizes
    |--------------------------------------------------------------------------
    |
    | Sizes for images to automatically create thumbnails for image.
    | Image will be fitted (resized and cropped) to given values, 
    | with no upsize, keeping aspect ratio.
    |
    */

    'thumbnail' => [
        'width'  => 240,
        'height' => 240,
    ],


    /*
    |--------------------------------------------------------------------------
    | Optimization callback
    |--------------------------------------------------------------------------
    |
    | You can defined optimization callback to optimize your uploaded image.
    | 
    | With spatie/laravel-image-optimizer package you may define the following
    | callback:
    | 
    |     'optimization' => function($image_file) {
    |         ImageOptimizer::optimize($image_file);
    |     },
    |
    */

    'optimization' => null,


    /*
    |--------------------------------------------------------------------------
    | Time limit
    |--------------------------------------------------------------------------
    |
    | Time limit to process image manipulations (previews, thumnails, 
    | optimizations, etc).
    |
    */

    'time_limit' => 60,


    /*
    |--------------------------------------------------------------------------
    | Temp path
    |--------------------------------------------------------------------------
    |
    | This option determines where all the uploaded files will be
    | stored.
    |
    */

    'temp_path' => storage_path('temp'),

];
