<?php

namespace Lamotivo\Uploads;

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use Image;
use Finfo;

use Flow\Config as FlowConfig;
use Flow\File as FlowFile;
use Flow\Request as FlowRequest;

use Ramsey\Uuid\Uuid;

class UploadHandler implements UploadHandlerInterface
{
    protected $disk;
    protected $diskname;
    protected $disk_config;
    protected $presizes = [];
    protected $presize_disabled = false;
    protected $time_limit = 60;
    protected $temp_path;
    protected $optimization;
    protected $sizes_resize;
    protected $sizes_preview;
    protected $sizes_thumbnail;

    public function __construct($diskname, $disk, $config)
    {
        $this->diskname = $diskname;
        $this->disk = $disk;
        $this->disk_config = config('filesystems.disks.' . $diskname);

        $this->presizes = Arr::get($config, 'presizes', []);
        $this->presize_disabled = Arr::get($config, 'presize_disabled', false);

        $this->time_limit = Arr::get($config, 'time_limit', 60);

        $this->temp_path = Arr::get($config, 'temp_path', storage_path('temp'));

        $this->optimization = Arr::get($config, 'optimization');

        $this->sizes_resize = Arr::get($config, 'resize');
        $this->sizes_preview = Arr::get($config, 'preview');
        $this->sizes_thumbnail = Arr::get($config, 'thumbnail');
    }

    public function getFlowConfig()
    {
        return new FlowConfig([
            'tempDir' => $this->temp_path,
        ]);
    }

    public function disk()
    {
        return Storage::disk($this->diskname);
    }

    public function uploadsPath($path)
    {
        $split_dir = substr($path, 0, 2);
        return $split_dir . '/' . $path;
    }

    protected function putFile($uuid, $path, $srcfile, $filename)
    {
        if ($path)
        {
            $path = trim($path, '/') . '/';
        }

        $filepath = $this->disk()->putFileAs(
            $path . $this->uploadsPath($uuid),
            new File($srcfile),
            $filename
        );

        return [
            'uuid' => $uuid,
            'filename' => $filename,
            'filepath' => $filepath,
        ];
    }

    public function upload($tmpFile, $uuid, $filename, $resize = null, $path = null)
    {
        set_time_limit($this->time_limit);

        $disk = $this->disk();
        $disk_info = $this->disk;

        if ($path)
        {
            $path = trim($path, '/') . '/';
        }

        $width  = null;
        $height = null;

        $thumbnail = 'thumbnail-' . $filename;
        $preview   = 'preview-' . $filename;

        $tmpFilePreview   = $this->temp_path . '/uploader-tmp-' . $uuid . '-preview-' . $filename;
        $tmpFileThumbnail = $this->temp_path . '/uploader-tmp-' . $uuid . '-thumbnail-' . $filename;

        if (Str::startsWith($tmpFile, 'http://') || Str::startsWith($tmpFile, 'https://'))
        {
            $sourceUrl = $tmpFile;
            $tmpFile = $this->temp_path . '/uploader-tmp-' . $uuid . '-' . $filename;

            @mkdir(dirname($tmpFile), 0777, true);

            if ( ! $this->downloadFile($sourceUrl, $tmpFile))
            {
                return false;
            }
        }

        $mime_type = $this->getMimeType($tmpFile);
        $type = $this->getType($mime_type);

        if (!preg_match('/\.\w+$/', $filename))
        {
            $ext = $this->getExt($mime_type);
            $filename .= '.' . $ext;
        }

        if ($type === 'image')
        {
            $sizes = getimagesize($tmpFile);

            if ($sizes)
            {
                $width  = $sizes[0];
                $height = $sizes[1];

                $img = Image::make($tmpFile);
                $img->orientate();

                $img->backup();

                if ($resize)
                {
                    $this->resize(
                        $img,
                        Arr::get($resize, 'size'),
                        Arr::get($resize, 'method', 'resize')
                    );
                }
                else
                {
                    $this->resize($img, '', 'resize');
                }

                $width  = $img->width();
                $height = $img->height();

                $img->save($tmpFile);

                $img->reset();

                $optimization = $this->optimization;

                if ($optimization)
                {
                    $optimization($tmpFile);
                }

                $file = $this->putFile($uuid, $path, $tmpFile, $filename);

                // applying resizes (previews, thumbnails, etc custom)

                if (!$this->presize_disabled)
                {
                    $this->createPreview(clone $img, $tmpFilePreview);
                    $this->createThumbnail(clone $img, $tmpFileThumbnail);

                    $presizes = [];

                    if ($this->presizes)
                    {
                        foreach ($this->presizes as $size => $props)
                        {
                            $presizeFile = $this->temp_path . '/uploader-tmp-' . $uuid . '-' . $size .'-' . $filename;

                            $this->createCustomPresize(clone $img, $presizeFile, $size);

                            $sizeName = $size . '-' . $filename;

                            $presizes[$sizeName] = $presizeFile;
                        }
                    }

                    if ($optimization)
                    {
                        $optimization($tmpFilePreview);
                        $optimization($tmpFileThumbnail);

                        foreach ($presizes as $presizeFile)
                        {
                            $optimization($presizeFile);
                        }
                    }

                    $this->putFile($uuid, $path, $tmpFilePreview, $preview);
                    $this->putFile($uuid, $path, $tmpFileThumbnail, $thumbnail);

                    unlink($tmpFilePreview);
                    unlink($tmpFileThumbnail);

                    foreach ($presizes as $sizeName => $presizeFile)
                    {
                        $this->putFile($uuid, $path, $presizeFile, $sizeName);
                        unlink($presizeFile);
                    }
                }
            }
            else
            {
                $thumbnail = null;
                $preview = null;
                $file = $this->putFile($uuid, $path, $tmpFile, $filename);
            }
        }
        else
        {
            $thumbnail = null;
            $preview = null;
            $file = $this->putFile($uuid, $path, $tmpFile, $filename);
        }

        $filesize = isset($file['size']) ? $file['size'] : filesize($tmpFile);

        unlink($tmpFile);

        $result = [
            'disk'      => $this->diskname,
            'filename'  => $file['filename'],
            'size'      => $filesize,
            'uuid'      => $file['uuid'],
            'path'      => $file['filepath'],
            'mime_type' => $mime_type,
            'type'      => $type,
            'width'     => (int)$width,
            'height'    => (int)$height,
        ];

        if ($preview)
        {
            $result['preview_path'] = $path . $this->uploadsPath($uuid . '/' . $preview);
        }

        if ($thumbnail)
        {
            $result['thumbnail_path'] = $path . $this->uploadsPath($uuid . '/' . $thumbnail);
        }

        return $result;
    }

    protected function downloadFile($url, $dst)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYSTATUS, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);

        $response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status == 200 && $response)
        {
            file_put_contents($dst, $response);
            return true;
        }

        return false;
    }

    public function url($attachment, $prefix = '')
    {
        $path = $this->uploadsPath($attachment->uuid . '/' . ($prefix ? $prefix . '-' : '') . $attachment->filename);

        $driver = config('filesystems.disks.' . $this->diskname . '.driver');

        if ($this->disk_config['driver'] === 'ftp')
        {
            $url = $this->disk_config['url'];
            return rtrim($url, '/') . '/' . $path;
        }
        else
        {
            return $this->disk()->url($path);
        }
    }

    public function store($attachment)
    {
        $attachment->store();
    }

    public function delete($attachment)
    {
        $this->disk()->deleteDirectory(dirname($attachment->path));
        $attachment->delete();
    }




    public function createPreview($img, $filename)
    {
        $width  = Arr::get($this->sizes_preview, 'width', 480);
        $height = Arr::get($this->sizes_preview, 'height', 480);

        if ($img->width() > $width || $img->height() > $height)
        {
            $img->resize($width, $height, function ($constraint) {
                $constraint->upsize();
                $constraint->aspectRatio();
            });
        }

        $img->save($filename);
    }

    public function createThumbnail($img, $filename)
    {
        $width  = Arr::get($this->sizes_thumbnail, 'width', 240);
        $height = Arr::get($this->sizes_thumbnail, 'height', 240);

        $img->fit($width, $height, function ($constraint) {
            $constraint->upsize();
        });

        $img->save($filename);
    }

    public function createCustomPresize($img, $filename, $size)
    {
        $width  = Arr::get($this->presizes[$size], 'width', 0);
        $height = Arr::get($this->presizes[$size], 'height', 0);
        $crop = Arr::get($this->presizes[$size], 'crop', false);

        if ($width && $height)
        {
            if ($crop)
            {
                $img->fit($width, $height, function ($constraint) {
                    $constraint->upsize();
                });
            }
            else
            {
                if ($img->width() > $width || $img->height() > $height)
                {
                    $img->resize($width, $height, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    });
                }
            }
            $img->save($filename);
        }
    }

    public function resize($img, $size = '', $method = 'resize')
    {
        $size = $this->getSize($size);

        $width = $img->width();
        $height = $img->height();

        // if image it too small, adjusting canvas
        if ($width < $size['width'] || $height < $size['height'])
        {
            switch ($method)
            {
                case 'check':
                case 'fit':
                case 'contain':
                    $img->resizeCanvas($size['width'], $size['height']);
                    break;
            }
            return;
        }

        // if image is larger than desired values, downscale it
        if ($width > $size['width'] || $height > $size['height'])
        {
            switch ($method)
            {
                case 'fit':
                case 'contain':
                    $img->fit($size['width'], $size['height']);
                    break;

                case 'resize':
                case 'cover':
                default:
                    $img->resize($size['width'], $size['height'], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    break;
            }
        }
    }



    protected function getSize($size)
    {
        if (is_string($size))
        {
            $size = explode('x', $size);
        }

        $size[0] = empty($size[0])
            ? Arr::get($this->sizes_resize, 'max_width', 1920)
            : (int)$size[0];

        $size[1] = empty($size[1])
            ? Arr::get($this->sizes_resize, 'max_height', 1920)
            : (int)$size[1];

        return [
            'width'  => $size[0],
            'height' => $size[1],
        ];
    }

    protected function finfo()
    {
        return new Finfo(FILEINFO_MIME_TYPE);
    }

    protected function getMimeType($file)
    {
        return $this->finfo()->file($file);
    }

    protected function getType($mime_type)
    {
        $mime_type = strtolower($mime_type);

        list($type, $subtype) = explode('/', $mime_type);

        switch ($subtype)
        {
          case 'pdf':
          case 'excel':
          case 'msword':
          case 'powerpoint':
          case 'vnd.ms-excel':
          case 'vnd.ms-powerpoint':
          case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
          case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
          case 'vnd.openxmlformats-officedocument.presentationml.presentation':
          case 'vnd.oasis.opendocument.spreadsheet':
          case 'vnd.oasis.opendocument.text':
          case 'vnd.oasis.opendocument.presentation':
          case 'x-photoshop':
          case 'vnd.adobe.photoshop':
          case 'svg+xml':
            return 'document';
        }

        switch ($type)
        {
          case 'audio':
            return 'audio';

          case 'video':
            return 'video';

          case 'image':
            switch ($subtype)
            {
              case 'jpeg':
              case 'pjpeg':
              case 'gif':
              case 'png':
                return 'image';
            }
            return 'document';

          case 'text':
            return 'text';
            return;
        }

        return 'other';
    }

    protected function getExt($mime_type)
    {
        $mime_type = strtolower($mime_type);

        switch ($mime_type)
        {
            case 'text/html': return 'html';
            case 'text/css': return 'css';
            case 'text/xml': return 'xml';
            case 'image/gif': return 'gif';
            case 'image/jpeg': return 'jpg';
            case 'application/javascript': return 'js';
            case 'application/atom+xml': return 'atom';
            case 'application/rss+xml': return 'rss';

            case 'text/mathml': return 'mml';
            case 'text/plain': return 'txt';
            case 'text/vnd.sun.j2me.app-descriptor': return 'jad';
            case 'text/vnd.wap.wml': return 'wml';
            case 'text/x-component': return 'htc';

            case 'image/png': return 'png';
            case 'image/tiff': return 'tif';
            case 'image/vnd.wap.wbmp': return 'wbmp';
            case 'image/x-icon': return 'ico';
            case 'image/x-jng': return 'jng';
            case 'image/x-ms-bmp': return 'bmp';
            case 'image/svg+xml': return 'svg';
            case 'image/webp': return 'webp';

            case 'application/font-woff': return 'woff';
            case 'application/java-archive': return 'jar';
            case 'application/json': return 'json';
            case 'application/mac-binhex40': return 'hqx';
            case 'application/msword': return 'doc';
            case 'application/pdf': return 'pdf';
            case 'application/postscript': return 'ps';
            case 'application/rtf': return 'rtf';
            case 'application/vnd.apple.mpegurl': return 'm3u8';
            case 'application/vnd.ms-excel': return 'xls';
            case 'application/vnd.ms-fontobject': return 'eot';
            case 'application/vnd.ms-powerpoint': return 'ppt';
            case 'application/vnd.wap.wmlc': return 'wmlc';
            case 'application/vnd.google-earth.kml+xml': return 'kml';
            case 'application/vnd.google-earth.kmz': return 'kmz';
            case 'application/x-7z-compressed': return '7z';
            case 'application/x-cocoa': return 'cco';
            case 'application/x-java-archive-diff': return 'jardiff';
            case 'application/x-java-jnlp-file': return 'jnlp';
            case 'application/x-rar-compressed': return 'rar';
            case 'application/x-redhat-package-manager': return 'rpm';
            case 'application/x-sea': return 'sea';
            case 'application/x-shockwave-flash': return 'swf';
            case 'application/x-stuffit': return 'sit';
            case 'application/x-tcl': return 'tcl';
            case 'application/x-xpinstall': return 'xpi';
            case 'application/xhtml+xml': return 'xhtml';
            case 'application/xspf+xml': return 'xspf';
            case 'application/zip': return 'zip';

            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': return 'docx';
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': return 'xlsx';
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': return 'pptx';

            case 'audio/midi': return 'mid';
            case 'audio/mpeg': return 'mp3';
            case 'audio/ogg': return 'ogg';
            case 'audio/x-m4a': return 'm4a';
            case 'audio/x-realaudio': return 'ra';

            case 'video/3gpp': return '3gp';
            case 'video/mp2t': return 'ts';
            case 'video/mp4': return 'mp4';
            case 'video/mpeg': return 'mpg';
            case 'video/quicktime': return 'mov';
            case 'video/webm': return 'webm';
            case 'video/x-flv': return 'flv';
            case 'video/x-m4v': return 'm4v';
            case 'video/x-mng': return 'mng';
            case 'video/x-ms-asf': return 'asf';
            case 'video/x-ms-wmv': return 'wmv';
            case 'video/x-msvideo': return 'avi';
        }

        return '';
    }

}
