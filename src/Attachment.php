<?php

namespace Lamotivo\Uploads;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model implements AttachmentContract
{
  use SoftDeletes;

  const TYPE_IMAGE    = 1;
  const TYPE_TEXT     = 2;
  const TYPE_DOCUMENT = 3;
  const TYPE_VIDEO    = 4;
  const TYPE_AUDIO    = 5;
  const TYPE_OTHER    = 0;

  const STATUS_UPLOADED = 0;
  const STATUS_STORED   = 1;
  const STATUS_INLINE   = 2;
  const STATUS_MISSED   = 7;
  const STATUS_ARCHIVED = 8;
  const STATUS_DELETED  = 9;

  protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at',
  ];

  protected $fillable = [
    'disk',
    'directory',
    'filename',
    'size',
    'uuid',
    'mime_type',
    'type',
    'status',
    'sort',
    'width',
    'height',
    'user_id',
    'user_type',
    'attachable_id',
    'attachable_type',
    'inlinable_id',
    'inlinable_type',
  ];

  protected $hidden = [
    'attachable_id',
    'attachable_type',
    'inlinable_id',
    'inlinable_type',
    'deleted_at',
    'updated_at',
    'user_id',
    'user_type',
  ];

  protected $casts = [
    'size'          => 'integer',
    'status'        => 'integer',
    'sort'          => 'integer',
    'sizetype'      => 'integer',
    'width'         => 'integer',
    'height'        => 'integer',
    'user_id'       => 'integer',
    'attachable_id' => 'integer',
    'inlinable_id'  => 'integer',
  ];

  protected $appends = [
    'url',
    // 'path',
    'preview',
    'thumbnail',
    'human_size',
  ];

  public function user()
  {
    if (config('uploads.user_morphing'))
    {
      return $this->morphTo();
    }
    else
    {
      return $this->belongsTo(config('uploads.user_model', \App\User::class));
    }
  }

  public function attachable()
  {
    return $this->morphTo();
  }

  public function inlinable()
  {
    return $this->morphTo();
  }

  public static function findUuid($uuid)
  {
    return static::where('uuid', $uuid)->find();
  }

  public function setTypeAttribute($value)
  {
    switch ($value)
    {
      case 'image':    $value = static::TYPE_IMAGE; break;
      case 'text':     $value = static::TYPE_TEXT; break;
      case 'document': $value = static::TYPE_DOCUMENT; break;
      case 'video':    $value = static::TYPE_VIDEO; break;
      case 'audio':    $value = static::TYPE_AUDIO; break;
      case 'other':    $value = static::TYPE_OTHER; break;
    }

    $this->attributes['type'] = (int)$value;
  }

  public function getTypeAttribute()
  {
    $value = Arr::get($this->attributes, 'type');

    switch ($value)
    {
      case static::TYPE_IMAGE:    $value = 'image'; break;
      case static::TYPE_TEXT:     $value = 'text'; break;
      case static::TYPE_DOCUMENT: $value = 'document'; break;
      case static::TYPE_VIDEO:    $value = 'video'; break;
      case static::TYPE_AUDIO:    $value = 'audio'; break;
      case static::TYPE_OTHER:    $value = 'other'; break;
    }

    return $value;
  }

  public function setMimeTypeAttribute($value)
  {
    $value = strtolower($value);
    $this->attributes['mime_type'] = $value;
    list($type, $subtype) = explode('/', $value);
    switch ($subtype)
    {
      case 'pdf':
      case 'excel':
      case 'msword':
      case 'powerpoint':
      case 'vnd.ms-excel':
      case 'vnd.ms-powerpoint':
      case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
      case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      case 'vnd.openxmlformats-officedocument.presentationml.presentation':
      case 'vnd.oasis.opendocument.spreadsheet':
      case 'vnd.oasis.opendocument.text':
      case 'vnd.oasis.opendocument.presentation':
      case 'x-photoshop':
      case 'vnd.adobe.photoshop':
      case 'svg+xml':
        $this->attributes['type'] = static::TYPE_DOCUMENT;
        return;
    }

    switch ($type)
    {
      case 'audio': $this->attributes['type'] = static::TYPE_AUDIO; return;
      case 'video': $this->attributes['type'] = static::TYPE_VIDEO; return;
      case 'image':
        switch ($subtype)
        {
          case 'jpeg':
          case 'pjpeg':
          case 'gif':
          case 'png':
            $this->attributes['type'] = static::TYPE_IMAGE;
            return;
          default:
            $this->attributes['type'] = static::TYPE_DOCUMENT;
        }
        return;
      case 'text': $this->attributes['type'] = static::TYPE_TEXT; return;
    }

    $this->attributes['type'] = static::TYPE_OTHER;
  }

  public function getUrlAttribute()
  {
    return app('uploader')->url($this);
  }

  public function getPreviewAttribute()
  {
    if ($this->is_image())
    {
      return app('uploader')->url($this, 'preview');
    }

    return null;
  }

  public function getThumbnailAttribute()
  {
    if ($this->is_image())
    {
      return app('uploader')->url($this, 'thumbnail');
    }

    return null;
  }

  public function getResizedUrl($size)
  {
    if ($this->is_image())
    {
      return app('uploader')->url($this, $size);
    }

    return null;
  }

  public function getPathAttribute()
  {
    return app('uploader')->uploadsPath($this->disk, $this->uuid . '/' . $this->filename);
  }

  public function getHumanSizeAttribute()
  {
    return human_size($this->size);
  }


  public function is_image()
  {
    return Arr::get($this->attributes, 'type') == static::TYPE_IMAGE;
  }

  public function is_text()
  {
    return Arr::get($this->attributes, 'type') == static::TYPE_TEXT;
  }

  public function is_document()
  {
    return Arr::get($this->attributes, 'type') == static::TYPE_DOCUMENT;
  }

  public function is_video()
  {
    return Arr::get($this->attributes, 'type') == static::TYPE_VIDEO;
  }

  public function is_audio()
  {
    return Arr::get($this->attributes, 'type') == static::TYPE_AUDIO;
  }

  public function scopeStored($query)
  {
    return $query->where('status', static::STATUS_STORED);
  }


  public function scopeImages($query)
  {
    return $query->where('type', static::TYPE_IMAGE);
  }

  public function scopeTexts($query)
  {
    return $query->where('type', static::TYPE_TEXT);
  }

  public function scopeDocuments($query)
  {
    return $query->where('type', static::TYPE_DOCUMENT);
  }

  public function scopeVideos($query)
  {
    return $query->where('type', static::TYPE_VIDEO);
  }

  public function scopeAudios($query)
  {
    return $query->where('type', static::TYPE_AUDIO);
  }


  public function scopeOfType($query, $type)
  {
    switch ($type)
    {
      case 'image':    $query->where('type', static::TYPE_IMAGE); break;
      case 'text':     $query->where('type', static::TYPE_TEXT); break;
      case 'document': $query->where('type', static::TYPE_DOCUMENT); break;
      case 'video':    $query->where('type', static::TYPE_VIDEO); break;
      case 'audio':    $query->where('type', static::TYPE_AUDIO); break;
      case 'other':    $query->where('type', static::TYPE_OTHER); break;
    }

    return $query;
  }


  public function store()
  {
    if ($this->status != static::STATUS_STORED)
    {
      return $this->update(['status' => static::STATUS_STORED]);
    }
  }


}
