<?php

namespace Lamotivo\Uploads;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class UploadServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $commands = [
        // 'Lamotivo\Uploads\Commands\Prune',
    ];

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot(Filesystem $filesystem)
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/uploads.php', 'uploads');

        if ($this->app->runningInConsole())
        {
            $this->publishes([
                __DIR__ . '/../config/uploads.php' => config_path('uploads.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../database/migrations/create_attachments_table.php.stub' => $this->getMigrationFileName($filesystem),
            ], 'migrations');
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerUploader();
        $this->registerUploaderRoutes();
        $this->commands($this->commands);
    }


    /**
     * Register the Lamotivo Uploader instance.
     *
     * @return void
     */
    protected function registerUploader()
    {
        $this->app->singleton('uploader', function ($app) {
            $config = $app['config']->get('uploads');

            $uploader = new Uploader($config);

            return $uploader;
        });
        $this->app->bind(UploaderContract::class, 'uploader');
    }

    /**
     * Register the Uploader routes.
     *
     * @return void
     */
    public function registerUploaderRoutes()
    {
        Route::prefix(config('uploads.routes.prefix', 'vendor-api/upload'))
             ->middleware(config('uploads.routes.middleware', ['web']))
             ->namespace('Lamotivo\Uploads\Controllers')
             ->group(function() {
                Route::get('/', 'UploadController@index');
                Route::post('/', 'UploadController@store');
                Route::delete('{id}', 'UploadController@destroy');
             });
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem)
    {
        $timestamp = date('Y_m_d_His');
        return Collection::make($this->app->databasePath().'/migrations/')
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path.'*_create_attachments_table.php');
            })->push($this->app->databasePath()."/migrations/{$timestamp}_create_attachments_table.php")
            ->first();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['uploader'];
    }

}

