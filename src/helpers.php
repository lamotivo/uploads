<?php

if (! function_exists('get_size_units'))
{
    function get_size_units()
    {
        // return ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        return ['Б', 'КБ', 'МБ', 'ГБ', 'ТБ', 'ПБ', 'EB', 'ЗБ', 'ИБ'];
    }
}

if (! function_exists('human_size'))
{
    function human_size($size)
    {
        $units = get_size_units();

        $factor = (int)floor((strlen($size) - 1) / 3);

        $number = number_format($size / pow(1024, $factor), 2, ',', ' ');

        $unit = isset($units[$factor]) ? $units[$factor] : '*';

        return $number . ' ' . $unit;
    }
}
