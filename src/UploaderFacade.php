<?php

namespace Lamotivo\Uploads;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Lamotivo\Uploads\Uploader
 */
class UploaderFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'uploader';
    }
}
