<?php

namespace Lamotivo\Uploads;

use Finfo;

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

use Image;

use Flow\Config as FlowConfig;
use Flow\File as FlowFile;
use Flow\Request as FlowRequest;

use Ramsey\Uuid\Uuid;

class Uploader
{
    protected $default_disk;

    protected $disk;

    protected $disks = [];

    protected $attachment_model;

    protected $time_limit = 60;

    protected $temp_path;

    protected $optimization;

    protected $sizes_resize;

    protected $sizes_preview;

    protected $sizes_thumbnail;

    public function __construct($config)
    {
        $this->attachment_model = Arr::get($config, 'attachment_model');

        if ( ! $this->attachment_model)
        {
            throw new UploaderException('No attachment model configured');
        }

        $this->default_disk = Arr::get($config, 'disk');

        if ( ! $this->default_disk)
        {
            throw new UploaderException('Default disk is not configured');
        }

        $this->disks = Arr::get($config, 'disks');

        if ( ! $this->disks)
        {
            throw new UploaderException('No disk configured');
        }

        foreach ($this->disks as $diskname => $disk)
        {
            if ($handler = Arr::get($disk, 'handler'))
            {
                $this->disks[$diskname]['handler'] = new $handler($diskname, $disk, $config);
            }
            else
            {
                $this->disks[$diskname]['handler'] = new UploadHandler($diskname, $disk, $config);
            }
        }

        $this->time_limit = Arr::get($config, 'time_limit', 60);

        $this->temp_path = Arr::get($config, 'temp_path', storage_path('temp'));

        $this->optimization = Arr::get($config, 'optimization');

        $this->sizes_resize = Arr::get($config, 'resize');
        $this->sizes_preview = Arr::get($config, 'preview');
        $this->sizes_thumbnail = Arr::get($config, 'thumbnail');

        $this->setDefaultDisk();
    }

    public function model()
    {
        return $this->attachment_model;
    }

    public function query($conditions = null)
    {
        $attachment_model = $this->attachment_model;
        $query = (new $attachment_model)->newQuery();

        if ($conditions)
        {
            $query->where($conditions);
        }

        return $query;
    }

    public function getFlowConfig()
    {
        return new FlowConfig([
            'tempDir' => $this->temp_path,
        ]);
    }

    public function disks()
    {
        return $this->disks;
    }

    public function setDefaultDisk($disk = null)
    {
        if ($disk && !isset($this->disks[$disk]))
        {
            throw new UploaderException('Disk [' . $disk . '] is not configured');
        }

        if (!$disk)
        {
            $disk = $this->default_disk;
        }

        $this->disk = Storage::disk($disk);
    }

    public function disk($disk = null)
    {
        if ($disk && !isset($this->disks[$disk]))
        {
            throw new UploaderException('Disk [' . $disk . '] is not configured');
        }

        if (!$disk)
        {
            return $this->disk;
        }

        return Storage::disk($disk);
    }

    public function diskConfig($disk)
    {
        if (!isset($this->disks[$disk]))
        {
            throw new UploaderException('Disk [' . $disk . '] is not configured');
        }

        return $this->disks[$disk];
    }

    public function diskHandler($disk)
    {
        if (!isset($this->disks[$disk]))
        {
            throw new UploaderException('Disk [' . $disk . '] is not configured');
        }

        return $this->disks[$disk]['handler'];
    }

    public function url($attachment, $prefix = '')
    {
        return $attachment->disk ? $this->diskHandler($attachment->disk)->url($attachment, $prefix) : '';
    }


    public function uploadsPath($disk, $path)
    {
        return $this->diskHandler($disk)->uploadsPath($path);
    }

    public function createAttachment($data)
    {
        $attachment_model = $this->attachment_model;
        return $attachment_model::create($data);
    }

    public function findAttachment($find)
    {
        $attachment_model = $this->attachment_model;
        return (new $attachment_model)->newQuery()->find($find);
    }

    public function getAttachment($id)
    {
        if ($id instanceof AttachmentContract)
        {
            $attachment = $id;
        }
        else
        {
            $attachment_model = $this->attachment_model;
            $attachment = (new $attachment_model)->newQuery()->where('id', $id)->first();
        }

        return $attachment;
    }

    public function storeAttachment($id)
    {
        if (empty($id))
        {
            return;
        }

        if ($attachment = $this->getAttachment($id))
        {
            $this->diskHandler($attachment->disk)->store($attachment);
        }
    }

    public function deleteAttachment($id)
    {
        if (empty($id))
        {
            return;
        }

        if ($attachment = $this->getAttachment($id))
        {
            $this->diskHandler($attachment->disk)->delete($attachment);
        }
    }

    public function createFile($diskname, $tmpFile, $uuid, $filename, $resize = null, $path = null)
    {
        $disk_handler = $this->diskHandler($diskname);
        return $disk_handler->upload($tmpFile, $uuid, $filename, $resize, $path);
    }

    public function createFileFromUrl($url, $resize = null, $diskname = null, $path = null)
    {
        $uuid = (string)Uuid::uuid4();

        $filename = $this->fixFilename(basename($url));

        if (!$diskname)
        {
            $diskname = $this->default_disk;
        }

        return $this->createFile($diskname, $url, $uuid, $filename, $resize, $path);
    }

    public function createFileFromFlow(FlowFile $file, FlowRequest $flow, $resize = null, $diskname = null, $path = null)
    {
        $uuid = (string)Uuid::uuid4();

        $filename = $this->fixFilename($flow->getFileName());
        // $filesize = $flow->getTotalSize();

        $tmpFile = $this->temp_path . '/uploader-tmp-' . $uuid . '-' . $filename;

        @mkdir(dirname($tmpFile), 0777, true);

        $file->save($tmpFile);

        if (!$diskname)
        {
            $diskname = $this->default_disk;
        }

        return $this->createFile($diskname, $tmpFile, $uuid, $filename, $resize, $path);
    }

    public function createFileFromUploadedFile(UploadedFile $file, $resize = null, $diskname = null, $path = null)
    {
        $uuid     = (string)Uuid::uuid4();
        $filename = $this->fixFilename($file->getClientOriginalName());
        // $filesize = $file->getClientSize();

        $tmpFile = $this->temp_path . '/uploader-tmp-' . $uuid . '-' . $filename;

        @mkdir(dirname($tmpFile), 0777, true);

        $file->move(dirname($tmpFile), basename($tmpFile));

        if (!$diskname)
        {
            $diskname = $this->default_disk;
        }

        return $this->createFile($diskname, $tmpFile, $uuid, $filename, $resize, $path);
    }


    protected function fixFilename($value)
    {
        $value = preg_replace('/\?.*$/', '', $value);
        return preg_replace_callback('#^(.*)(\.\w+)$#', function($matches)
        {
            $filename = Arr::get($matches, 1);
            $ext = Arr::get($matches, 2);
            return strtolower(Str::slug($filename) . $ext);
        }, $value);
    }

}
