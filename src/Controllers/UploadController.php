<?php

namespace Lamotivo\Uploads\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Lamotivo\Uploads\UploaderFacade as Uploader;

use Flow\Request as FlowRequest;
use Flow\File as FlowFile;

class UploadController extends Controller
{

    /**
     * Return status of an uploaded file.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $file = $this->getFlowFile($request);

        return response('', $file->checkChunk() ? 200 : 204);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('tafile'))
        {
            if (!$request->file('tafile')->isValid())
            {
                abort(400);
            }

            $file = Uploader::createFileFromUploadedFile($request->tafile);

            return $this->saveFile($request, $file);
        }

        $file = $this->getFlowFile($request);

        if (!$file->validateChunk())
        {
            return abort(400);
        } 

        $file->saveChunk();

        if ($file->validateFile())
        {

            $resize = null;

            if ($size = $request->get('fit'))
            {
                $resize = [
                    'method' => 'fit',
                    'size' => $size,
                ];
            }
            elseif ($size = $request->get('resize'))
            {
                $resize = [
                    'method' => 'resize',
                    'size' => $size,
                ];
            }

            $path = $request->get('path', null);

            $file = Uploader::createFileFromFlow($file, $flow, $resize, $request->get('disk'), $path);

            if ($request->has('basic'))
            {
                return response()->json($file);
            }
            else
            {
                return $this->saveFile($request, $file);
            }
        }
    }

    /**
     * Delete a file from storage.
     *
     * @param  Request  $request
     * @param  string $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $attachment_model = config('uploads.attachment_model');

        if ( ! $attachment_model)
        {
            throw new UploaderException('No attachment model configured');
        }

        $attachment = $attachment_model::findOrFail($id);

        $user = $request->user();

        if ($user && $attachment->user_id == $user->id)
        {
            $attachment->delete();
        }
        else if (!$attachment->user_id)
        {
            $attachment->delete();
        }

        return response()->json([], 204);
    }

    /**
     * Return Flow File from request.
     *
     * @param  Request  $request
     * @return FlowFile
     */
    protected function getFlowFile(Request $request)
    {
        $flow = new FlowRequest($request->all());

        return new FlowFile(Uploader::getFlowConfig(), $flow);
    }

    /**
     * Save a file in storage.
     *
     * @param  Request  $request
     * @param  array $file
     * @return Response
     */
    protected function saveFile(Request $request, $file)
    {
        $attachment_model = config('uploads.attachment_model');

        if ( ! $attachment_model)
        {
            throw new UploaderException('No attachment model configured');
        }

        $user = $request->user();

        $attachment = new $attachment_model($file);

        if ($user)
        {
            $attachment->user()->associate($user);
        }

        if ($request->get('directory'))
        {
            $attachment->directory = $request->get('directory');
        }

        $attachment->save();

        if ($request->has('store'))
        {
            Uploader::storeAttachment($attachment);
        }

        return response()->json([
            'status' => 'File upload was completed',
            'attachment' => $attachment
        ], 200);
    }

}
