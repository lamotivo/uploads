<?php

namespace Lamotivo\Uploads;

interface AttachmentContract
{
    public function store();
}
